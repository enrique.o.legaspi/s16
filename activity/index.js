let n1 = Number(prompt("Enter a number: "));
for (let i = n1; i > 0; i--){
	if (i % 10 === 0){
		console.log("Number " + i + " is being skipped, loop will continue to next iteration");
		continue;
	}
	if (i % 5 === 0){
		console.log(i);
	}
	if (i <= 50){
		break;
	}
}

let str1 = "supercalifragilisticexpialidocious";
let strCons = "";
for (let i = 0; i < str1.length; i++){
	if (
		str1[i].toLowerCase() == "a" ||
		str1[i].toLowerCase() == "e" ||
		str1[i].toLowerCase() == "i" ||
		str1[i].toLowerCase() == "o" ||
		str1[i].toLowerCase() == "u"
		){

		continue

		}
	else {
		strCons += str1[i]
	}	
}
console.log(strCons);